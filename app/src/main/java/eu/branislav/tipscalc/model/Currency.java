package eu.branislav.tipscalc.model;

/**
 * Created by Branecko on 31.5.2015.
 */

public class Currency {

    private String name;
    private double value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
