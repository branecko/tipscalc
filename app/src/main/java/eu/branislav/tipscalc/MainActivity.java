package eu.branislav.tipscalc;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import eu.branislav.tipscalc.model.Currency;

/*
* Application has 2 features:
* #1 - exchangerate convertor for two currencies (USD, EUR)
*    - current exchange rate is downloaded from openexchangerates.org using okhttpclient
*
* #2 - tip calculator
*    - UI contains seekbar, calculates final price with tip in range <0%;25%> with step 5
*
* */

public class MainActivity extends ActionBarActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    // openexchangerates.org data
    private String apiKey = "f0747021abe841948a4c85c5b7377745";
    private String exchangeRateUrl = "https://openexchangerates.org/api/latest.json?app_id=" + apiKey;

    OkHttpClient mOkHttpClient = new OkHttpClient();

    //value of USD is 1 by default
    //currency EUR
    private Currency currencyEur;

    //convertor part
    TextView usdValue;
    TextView eurValue;
    Button toUsdButton;
    Button toEurButton;

    //tips calc part
    EditText tipsInput;
    SeekBar percSeekBar;
    TextView percValue;
    TextView tipValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usdValue = (TextView) findViewById(R.id.usd_rate_value);
        eurValue = (TextView) findViewById(R.id.eur_rate_value);
        toUsdButton = (Button) findViewById(R.id.convertToUsdButton);
        toEurButton = (Button) findViewById(R.id.convertToEurButton);

        tipsInput = (EditText) findViewById(R.id.TipInput);
        percSeekBar = (SeekBar) findViewById(R.id.tipSeekBar);
        percValue = (TextView) findViewById(R.id.percentageValue);
        tipValue = (TextView) findViewById(R.id.tipValue);

        toUsdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*
                * USD = EUR / EXRATE
                * */
                double val = Double.parseDouble(eurValue.getText()+"");
                double result = val / currencyEur.getValue();
                usdValue.setText(String.format( "%.2f", result )+"");
             }
        });

        toEurButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double val = Double.parseDouble(usdValue.getText()+"");
                double result = val * currencyEur.getValue();
                eurValue.setText(String.format( "%.2f", result )+"");
            }
        });

        percSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                //TODO: add condition if tipInput text is number (not empty)
                calculateTip(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        if (isNetworkAvailable()) {
            getExchangeRates();
        } else {
            //TODO: load exchange rates from sharedPreferences
            Toast.makeText(this, "No network available", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isAvailable()) {
            isAvailable = true;
        }
        return isAvailable;
    }

    public void getExchangeRates() {
        Request mRequest = new Request.Builder()
                .url(exchangeRateUrl)
                .build();
        Call mCall = mOkHttpClient.newCall(mRequest);
        mCall.enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
            }

            @Override
            public void onResponse(Response response) throws IOException {
                try {
                    String jsonData = response.body().string();
                    Log.v(TAG, jsonData);
                    if (response.isSuccessful()) {

                        currencyEur = getCurrentValue(jsonData);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //TODO: save exchange rates to sharedPreferences
                                updateDisplay();
                            }
                        });

                    } else {
                        //err
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "JSONException: " + e);
                }
            }
        });
    }

    private Currency getCurrentValue(String jsonData) throws JSONException {

        JSONObject currencyData = new JSONObject(jsonData);
        Log.d(TAG, jsonData);
        JSONObject rates = currencyData.getJSONObject("rates");
        double value = rates.getDouble("EUR");

        Currency currencyEur = new Currency();
        currencyEur.setValue(value);
        currencyEur.setName("EUR");

        return currencyEur;
    }

    private void updateDisplay() {
        eurValue.setText(String.format("%.2f", currencyEur.getValue() ) + "");
        usdValue.setText("1");
    }


    /*Tips calculator part*/
    private void calculateTip(int seekBarProgress){
        double val = Double.parseDouble(tipsInput.getText()+"");
        percValue.setText((seekBarProgress*5) + " %");
        tipValue.setText((val+val*seekBarProgress*5/100)+"");
    }

}